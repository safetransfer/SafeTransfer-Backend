package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	EthereumAddress string `gorm:"type:varchar(255);uniqueIndex"` // Specify the type and unique index with a defined length
	Nonce           string
}

package tests

import (
	"SafeTransfer/internal/db"
	"SafeTransfer/internal/model"
	"SafeTransfer/utils"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDatabaseMigration(t *testing.T) {
	// Set up a test database connection
	testDB := setupTestDatabase(t)
	defer testDB.Close()

	// Check if the migration was successful
	assertTableExists(t, testDB, "files")
	assertTableExists(t, testDB, "users")
}

func setupTestDatabase(t *testing.T) *db.Database {
	host := "db" // Use the service alias
	port := utils.GetEnvOrDefault("DB_PORT", "3306")
	dbname := utils.GetEnvOrDefault("DB_NAME", "testdb")
	user := utils.GetEnvOrDefault("DB_USER", "testuser")
	password := utils.GetEnvOrDefault("DB_PASSWORD", "testpassword")

	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", user, password, host, port, dbname)

	testDB, err := db.NewDatabase(dataSourceName)
	require.NoError(t, err, "failed to create test database")

	err = testDB.AutoMigrate(&model.File{}, &model.User{})
	require.NoError(t, err, "failed to migrate test database")

	return testDB
}
func assertTableExists(t *testing.T, db *db.Database, tableName string) {
	// Check if the specified table exists in the database
	exists := db.Migrator().HasTable(tableName)
	assert.True(t, exists, "table '%s' does not exist", tableName)
}

<?php
// Получение переменных среды для подключения к базе данных
$dbHost = getenv('DB_HOST');
$dbPort = getenv('DB_PORT');
$dbName = getenv('DB_NAME');
$dbUser = getenv('DB_USER');
$dbPassword = getenv('DB_PASSWORD');

// Пример сбора статистики
$stats = [
    'uptime' => shell_exec('uptime'),
    'memory_usage' => memory_get_usage(),
    'cpu_load' => sys_getloadavg(),
    'business_logic_stats' => [
        'total_files' => getTotalFiles($dbHost, $dbPort, $dbName, $dbUser, $dbPassword),
        'total_users' => getTotalUsers($dbHost, $dbPort, $dbName, $dbUser, $dbPassword),
    ]
];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['refresh'])) {
        // Refresh the statistics
        $stats = [
            'uptime' => shell_exec('uptime'),
            'memory_usage' => memory_get_usage(),
            'cpu_load' => sys_getloadavg(),
            'business_logic_stats' => [
                'total_files' => getTotalFiles($dbHost, $dbPort, $dbName, $dbUser, $dbPassword),
                'total_users' => getTotalUsers($dbHost, $dbPort, $dbName, $dbUser, $dbPassword),
            ]
        ];
    }
}

function getTotalFiles($dbHost, $dbPort, $dbName, $dbUser, $dbPassword) {
    // Подключение к базе данных и получение количества файлов
    $dsn = "mysql:host=$dbHost;port=$dbPort;dbname=$dbName;charset=utf8mb4";
    $conn = new PDO($dsn, $dbUser, $dbPassword);
    $query = $conn->query("SELECT COUNT(*) as file_count FROM files");
    return $query->fetch(PDO::FETCH_ASSOC)['file_count'];
}

function getTotalUsers($dbHost, $dbPort, $dbName, $dbUser, $dbPassword) {
    // Подключение к базе данных и получение количества пользователей
    $dsn = "mysql:host=$dbHost;port=$dbPort;dbname=$dbName;charset=utf8mb4";
    $conn = new PDO($dsn, $dbUser, $dbPassword);
    $query = $conn->query("SELECT COUNT(*) as user_count FROM users");
    return $query->fetch(PDO::FETCH_ASSOC)['user_count'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Statistics</title>
    <style>
        body {
            min-height: 100vh;
            background: linear-gradient(to bottom right, #3b82f6, #9333ea);
            display: flex;
            align-items: center;
            justify-content: center;
            margin: 0;
            font-family: Arial, sans-serif;
        }
        .container {
            background: white;
            padding: 2rem;
            border-radius: 0.5rem;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            text-align: center;
            max-width: 400px;
            width: 100%;
        }
        .title {
            font-size: 2rem;
            font-weight: bold;
            margin-bottom: 1rem;
            color: #1f2937;
        }
        .stat {
            font-size: 1rem;
            color: #4b5563;
            margin-bottom: 0.5rem;
        }
        .button {
            background: linear-gradient(to right, #3b82f6, #9333ea);
            color: white;
            font-weight: bold;
            padding: 1rem 2rem;
            border-radius: 9999px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            transition: background 0.3s ease-in-out;
            cursor: pointer;
        }
        .button:hover {
            background: linear-gradient(to right, #9333ea, #3b82f6);
        }
    </style>
</head>
<body>
    <div class="container">
        <h1 class="title">Statistics</h1>
        <div class="stat">Uptime: <?php echo htmlspecialchars($stats['uptime']); ?></div>
        <div class="stat">Memory Usage: <?php echo htmlspecialchars($stats['memory_usage']); ?> bytes</div>
        <div class="stat">CPU Load: <?php echo htmlspecialchars(implode(', ', $stats['cpu_load'])); ?></div>
        <div class="stat">Total Files: <?php echo htmlspecialchars($stats['business_logic_stats']['total_files']); ?></div>
        <div class="stat">Total Users: <?php echo htmlspecialchars($stats['business_logic_stats']['total_users']); ?></div>
        <form method="post">
            <button type="submit" name="refresh" class="button">Refresh</button>
        </form>
    </div>
</body>
</html>
